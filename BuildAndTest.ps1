function BuildAndTest
{
    param
    (
        [Parameter(Mandatory=$false)]
        [string] $path = "D:\Full Stack Application\full_stack\back_end",

        [Parameter(Mandatory=$true)]
        [string] $action,

        [Parameter(Mandatory=$true)]
        [string] $project        
    )

    process
    {
        if($action -eq "build")
        {
            if($project -eq "api")
            {
                Set-Location $path
                dotnet build
            }
            else {
                Write-Output "Project not found"
            }
        }
        
        if($action -eq "test")
        {
            if($project -eq "all")
            {
                Set-Location ($path + "\Tests")
                dotnet test            
            }
            
            if($project -eq "unit")
            {
                Set-Location ($path + "\Tests")
                dotnet test --filter TestCategory=UnitTets
            }
            
            if($project -eq "intergration")
            {
                Set-Location ($path + "\Tests")
                dotnet test --filter TestCategory=IntergrationTests
            }

            if($project -eq "angular")
            {
                Set-Location ($path + "\..\angular")
                ng test --watch=false
            }
        }

        Set-Location $path                
    }
}