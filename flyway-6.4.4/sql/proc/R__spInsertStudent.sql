CREATE OR ALTER PROCEDURE spInsertUser
@Username INT, 
@Name VARCHAR(50),
@Surname VARCHAR(50), 
@IdNumber VARCHAR(13), 
@Email VARCHAR(50), 
@GenderId INT,
@MaritalStatusId INT
AS
BEGIN
    INSERT INTO [User](Username, [Name], Surname, IdNumber, Email, GenderId, MaritalStatusId, [Password])
        VALUES(@Username, @Name, @Surname, @IdNumber, @Email, @GenderId, @MaritalStatusId, 'StartSmart');
END