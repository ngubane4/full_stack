CREATE PROCEDURE spUpdateAddress
    @Line1 varchar(255),
    @Line2 varchar(255),
    @City varchar(255),
    @Province varchar(100),
    @ZipCode int,
    @Username int
AS 
BEGIN
    update [Address] set Line1 = @Line1, Line2 = @Line2, City = @City, Province = @Province, ZipCode = @ZipCode 
        where Username = @Username
END