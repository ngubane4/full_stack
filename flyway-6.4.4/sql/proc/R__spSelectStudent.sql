CREATE OR AlTER PROCEDURE spSelectUser
@Username int
AS
BEGIN
	 SET NOCOUNT ON; 
    SELECT [User].[Username], [Name], [Surname], [IdNumber], [Email], [MaritalStatusId], [GenderId], [Password], [Role]
        FROM [User],[UserRole], [Role] WHERE [Role].[Id] = [UserRole].[RoleId] AND [UserRole].[Username]  = @Username AND [User].[Username] = @Username;
END