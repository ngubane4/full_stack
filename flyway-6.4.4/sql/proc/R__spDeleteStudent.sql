CREATE PROCEDURE spDeleteUser
@Username Int
AS
BEGIN
    DELETE FROM [UserRole] WHERE Username = @Username;
    DELETE FROM [Address] WHERE [Address].Username = @Username;
    Delete FROM [User] WHERE [Username] = @Username;
END