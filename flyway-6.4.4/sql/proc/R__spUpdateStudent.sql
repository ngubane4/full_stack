CREATE PROCEDURE spUpdateStudent
@Username INT, 
@Name VARCHAR(50),
@Surname VARCHAR(50), 
@IdNumber VARCHAR(13), 
@Email VARCHAR(50), 
@GenderId INT,
@MaritalStatusId INT
AS
BEGIN
    UPDATE [User] SET [Name] = @Name, [Surname] = @Surname, IdNumber = @IdNumber, Email = @Email,
        MaritalStatusId = @MaritalStatusId, GenderId = @GenderId  WHERE Username = @Username;
END