CREATE PROCEDURE spInsertAddress

@Line1 VARCHAR(255), 
@Line2 VARCHAR(255), 
@City VARCHAR(125), 
@province VARCHAR(100), 
@ZipCode INT,
@Username INT

AS
BEGIN
    INSERT INTO [Address] (Line1, Line2, City, Province, ZipCode, [Username])
        VALUES(@Line1, @Line2, @City, @province, @ZipCode, @Username);
END