CREATE PROCEDURE spSelectAllStudents
AS
BEGIN
    SELECT [Username], [Name], [Surname], [IdNumber], [Email], [MaritalStatusId], [GenderId]
        FROM [User];
END