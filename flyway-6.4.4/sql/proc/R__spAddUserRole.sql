CREATE PROCEDURE spAddUserRole
@RoleId INT,
@Username VARCHAR(8)
AS
BEGIN
    INSERT INTO [UserRole] VALUES (@username, @RoleId);
END

