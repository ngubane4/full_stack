CREATE PROCEDURE spGetAddress
    @Username int
AS
BEGIN
    SELECT [Id], [Line1], [Line2], [City], [Province], [ZipCode]
	    FROM [Address] WHERE [Username] = @Username;
END