CREATE TABLE [Address]
(
	[Id] INT IDENTITY PRIMARY KEY,
	[Line1] VARCHAR(255),
	[Line2] VARCHAR(255),
	[City] VARCHAR(125),
	[Province] VARCHAR(100),
	[ZipCode] INT,
	[Username] INT FOREIGN KEY REFERENCES [User](Username) NOT NULL
);   
