CREATE TABLE [User]
(
    [Username] INT PRIMARY KEY,
	[Name] VARCHAR(50),
	[Surname] VARCHAR(50),
	[IdNumber] VARCHAR(13),
	[Email] VARCHAR(50),
	[Password] VARCHAR(16),
    [MaritalStatusId] INT FOREIGN KEY REFERENCES [MaritalStatus](Id),
    [GenderId] INT FOREIGN KEY REFERENCES [Gender](Id)
);