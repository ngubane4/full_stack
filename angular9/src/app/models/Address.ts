export class Address {
    id: number;
    line1: string;
    line2: string;
    city: string;
    province: string;
    zipCode: number;
    username: number;
}
