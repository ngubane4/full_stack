export interface Subject{
    id: number;
    name: string;
    code: string;
    streamId: number;
}
