export class User {
   username: number;
   name: string;
   surname: string;
   idNumber: string;
   email: string;
   password: string;
   genderId: number;
   maritalStatusId: number;
   roleId: number;
}
