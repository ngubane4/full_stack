import {Component, OnInit} from '@angular/core';
import {Subject} from '../../models/Subject';
import {SubjectsService} from 'src/app/services/subjects.service';
import {AuthService} from '../../services/auth.service';

@Component({
    selector: 'app-sidenav',
    templateUrl: './sidenav.component.html',
    styleUrls: ['./sidenav.component.scss']
})

export class SidenavComponent implements OnInit {
    studentSubjects: Subject[];

    constructor(private subjectService: SubjectsService, private authService: AuthService) {
    }

    async ngOnInit() {
        this.studentSubjects = await this.subjectService.getStudentSubjects(this.authService.getUserName());
    }
}
