import { Component, OnInit, Inject } from '@angular/core';
import { User } from 'src/app/models/User';
import { StudentService } from 'src/app/services/student.service';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  selector: 'app-delete-user',
  templateUrl: './delete-user.component.html',
  styleUrls: ['./delete-user.component.scss']
})
export class DeleteUserComponent implements OnInit {

  constructor(@Inject(MAT_DIALOG_DATA) public data: User, private _studentService: StudentService, private _snackBar: MatSnackBar) {}

  ngOnInit() {
  }

  openSnackBar(message: string, action: string) {
    this._snackBar.open(message, action, {
      duration: 5000
    });
  }

  onDelete(){
    this._studentService.deleteStudent(this.data.username)
    .subscribe(data => {
      this.openSnackBar("Student delete", "Ok");
    });
  }
}
