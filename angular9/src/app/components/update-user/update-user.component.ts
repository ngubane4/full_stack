import {Component, OnInit, Inject} from '@angular/core';
import {Observable} from 'rxjs';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {StudentService} from 'src/app/services/student.service';
import {FormDataService} from 'src/app/services/form-data.service';
import {FormGroup, FormControl, Validators} from '@angular/forms';
import {User} from 'src/app/models/User';
import {MatSnackBar} from '@angular/material/snack-bar';
import {AddressService} from 'src/app/services/address.service';
import {Address} from 'src/app/models/Address';

@Component({
    selector: 'app-update-user',
    templateUrl: './update-user.component.html',
    styleUrls: ['./update-user.component.scss']
})
export class UpdateUserComponent implements OnInit {

    [x: string]: any;

    showFiller = false;
    isDarkTheme: Observable<boolean>;

    constructor(@Inject(MAT_DIALOG_DATA) public data: User,
                public dialogRef: MatDialogRef<UpdateUserComponent>,
                private _studentService: StudentService,
                private _snackBar: MatSnackBar,
                private _addressService: AddressService,
                public _formDataService: FormDataService) {
    }

    profileForm = new FormGroup({
        Name: new FormControl(this.data.name, [Validators.required, Validators.minLength(2)]),
        Surname: new FormControl(this.data.surname, [Validators.required, Validators.minLength(2)]),
        Email: new FormControl(this.data.email, [Validators.required, Validators.email]),
        IdNumber: new FormControl(this.data.idNumber, [Validators.required, Validators.pattern('^[0-9]{13}$')]),
        GenderId: new FormControl(this.data.genderId, Validators.required),
        MaritalStatusId: new FormControl(this.data.maritalStatusId, Validators.required),
        Username: new FormControl(this.data.username)
    });

    addressForm = new FormGroup({
        line1: new FormControl(null, Validators.required),
        line2: new FormControl(null, Validators.required),
        city: new FormControl(null, Validators.required),
        province: new FormControl(null, Validators.required),
        zipCode: new FormControl(null, Validators.required),
        studentId: new FormControl(null)
    });

    studentAddress: Address;

    ngOnInit() {
        this._addressService.getAddress(this.data.username).subscribe(address => {
            this.studentAddress = address;
            this.addressForm.setValue({
                line1: this.studentAddress.line1,
                line2: this.studentAddress.line2,
                city: this.studentAddress.city,
                province: this.studentAddress.province,
                zipCode: this.studentAddress.zipCode,
                studentId: this.studentAddress.username
            });
        });
    }

    onUpdateStudent() {
        this.studentAddress = this.addressForm.value;
        this.studentAddress.zipCode = Number(this.addressForm.get('zipCode').value);

        if (this.addressForm.dirty && this.profileForm.dirty) {
            this.updateStudentProfile();
            this.updateAddress();
        } else if (this.profileForm.dirty) {
            this.updateStudentProfile();
        } else {
            this.updateAddress();
        }
    }

    updateStudentProfile() {
        this._studentService.updateStudent(this.profileForm.value).subscribe((response) => {
                this.dialogRef.close();
                this.openSnackBar('Student information updated', 'Ok');
            },
            error => {
                this.openSnackBar('Student information not updated', 'OK');
            },
        );
    }

    updateAddress() {
        this._addressService.updateAddress(this.studentAddress).subscribe(() => {
            this.dialogRef.close();
            this.openSnackBar('Student information updated', 'Ok');
        }, error => {
            this.openSnackBar('An error has occured', 'Ok');
        });
    }

    openSnackBar(message: string, action: string) {
        this._snackBar.open(message, action, {
            duration: 5000
        });
    }
}
