import { Component, OnInit, Inject } from '@angular/core';
import { StudentService } from 'src/app/services/student.service';
import { AddressService } from 'src/app/services/address.service';
import { MatSnackBar } from '@angular/material/snack-bar';
import { FormDataService } from 'src/app/services/form-data.service';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { User } from 'src/app/models/User';
import { Address } from 'src/app/models/Address';

@Component({
  selector: 'app-register-student',
  templateUrl: './register-student.component.html',
  styleUrls: ['./register-student.component.scss']
})
export class RegisterStudentComponent implements OnInit {

  constructor(
    public dialogRef: MatDialogRef<RegisterStudentComponent>,
    private studentService: StudentService,
    private addressService: AddressService,
    private snackBar: MatSnackBar,
    public formDataService: FormDataService
  ) { }

  registerForm: FormGroup;
  student: User;
  studentRole;

  ngOnInit(): void {
    this.studentRole = this.formDataService.getSystemRolesList[8].id;
    this.registerForm = new FormGroup({
      name: new FormControl(null, [Validators.required, Validators.minLength(2)]),
      surname: new FormControl(null, [Validators.required, Validators.minLength(2)]),
      email: new FormControl(null, [Validators.required, Validators.email]),
      idNumber: new FormControl(null, [Validators.required, Validators.pattern('^[0-9]{13}$')]),
      gender: new FormControl(null, Validators.required),
      maritalStatus: new FormControl(null, Validators.required),
      userRole: new FormControl(null, Validators.required),
      line1: new FormControl(null, Validators.required),
      line2: new FormControl(null, Validators.required),
      city: new FormControl(null, Validators.required),
      province: new FormControl(null, Validators.required),
      zipCode: new FormControl(null, Validators.required)
    });
  }

  onSubmit() {
    this.student = {
      username: 1,
      name: this.registerForm.get('name').value,
      surname: this.registerForm.get('surname').value,
      idNumber: this.registerForm.get('idNumber').value,
      email: this.registerForm.get('email').value,
      genderId: this.registerForm.get('gender').value,
      maritalStatusId: this.registerForm.get('maritalStatus').value,
      roleId: 1,
      password: 'StartSmart',
    };

    this.studentService.addStudent(this.student).subscribe(data => {
      this.student.username = data.username;
      this.submitStudentAddress();
    });
  }

  submitStudentAddress() {
    const address: Address = {
      id: 0,
      line1: this.registerForm.get('line1').value,
      line2: this.registerForm.get('line2').value,
      city: this.registerForm.get('city').value,
      province: this.registerForm.get('province').value,
      zipCode: + this.registerForm.get('zipCode').value,
      username: this.student.username,
    };

    this.addressService.addAddress(address).subscribe(date => {
      this.dialogRef.close();
      this.openSnackBar('student registered successfully', 'Done');
    });
  }

  openSnackBar(message: string, action: string) {
    this.snackBar.open(message, action, {
      duration: 5000
    });
  }
}
