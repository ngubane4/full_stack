import { MatSnackBar } from '@angular/material/snack-bar';
import { Component, OnInit, Input } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { Router } from '@angular/router';
import { LogInModel } from 'src/app/models/LogInModel';
import { AuthService } from 'src/app/services/auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit{
  hide = true;
  logInForm: FormGroup;
  logInModel: LogInModel;

  constructor(private authService: AuthService, private router: Router, private snackBar: MatSnackBar) { }

  openSnackBar(message: string, action: string) {
    this.snackBar.open(message, action, {
      duration: 5000
    });
  }

  ngOnInit() {
    this.logInForm = new FormGroup({
      Username: new FormControl(null),
      Password: new FormControl(null),
    });
  }

  onLogIn() {
    this.logInModel = {
      Username: Number(this.logInForm.get('Username').value),
      Password: this.logInForm.get('Password').value
    };

    this.authService.logIn(this.logInModel).subscribe(
      res => {
        sessionStorage.setItem('token', res);
        this.router.navigate(['/dashboard']);
      },
      err => {
        this.openSnackBar('Ops', err.error);
      }
    );
  }
}
