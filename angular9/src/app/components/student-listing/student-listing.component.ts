import { SystemRoleService } from './../../services/system-role.service';
import { Component, OnInit, ViewChild } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { StudentService } from 'src/app/services/student.service';
import { User } from 'src/app/models/User';
import { MatDialog } from '@angular/material/dialog';
import { FormDataService } from 'src/app/services/form-data.service';
import { GenderService } from 'src/app/services/gender.service';
import { MaritalStatusService } from 'src/app/services/marital-status.service';
import { IDmanipulations } from 'src/app/helpers/IDmanipulation';
import { DeleteUserComponent } from '../delete-user/delete-user.component';
import { UpdateUserComponent } from '../update-user/update-user.component';
import { RegisterStudentComponent } from '../register-student/register-student.component';
import { AuthService } from 'src/app/services/auth.service';
import { ASTWithName } from '@angular/compiler';
import { saveAs } from 'file-saver';

@Component({
  selector: 'app-student-listing',
  templateUrl: './student-listing.component.html',
  styleUrls: ['./student-listing.component.scss']
})
export class StudentListingComponent implements OnInit {

  IDmanipulations = new IDmanipulations();
  dataSource: MatTableDataSource<User>;
  length = 100;
  pageSize = 10;
  pageSizeOptions: number[] = [5, 10, 25, 100];

  displayedColumns: string[] = [
    'Username',
    'Name',
    'Surname',
    'Age',
    'Gender',
    'EmailAddress',
    'Actions'
  ];

  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;

  constructor(
    private studentService: StudentService,
    public auth: AuthService,
    public dialog: MatDialog,
    public formDataService: FormDataService,
    private genderService: GenderService,
    private maritalStatuseService: MaritalStatusService,
    private systemRoleService: SystemRoleService
  ) { }

  ngOnInit() {
    this.onRefreshPage();
    this.auth.getUserRole();
  }

  async onRefreshPage() {
    const students = await this.studentService.getStudents();
    this.formDataService.setMaritalStatusList = await this.maritalStatuseService.getMaritalStatuses();
    this.formDataService.setGenderList = await this.genderService.getGenders();
    this.formDataService.setSystemRoleList = await this.systemRoleService.getSystemRoles();

    this.dataSource = new MatTableDataSource(students);
    this.dataSource.sort = this.sort;
    this.dataSource.paginator = this.paginator;
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  onDeleteStudent(student: User) {
    const dialogRef = this.dialog.open(DeleteUserComponent, {
      data: student
    });

    dialogRef.afterClosed().subscribe(result => {
      this.onRefreshPage();
    });
  }

  onUpdateStudent(student: User) {
    const dialogRef = this.dialog.open(UpdateUserComponent, {
      width: '40vw',
      data: student
    });

    dialogRef.afterClosed().subscribe(result => {
      this.onRefreshPage();
    });
  }

  onAddStudents(): void {
    const dialogRef = this.dialog.open(RegisterStudentComponent, {
      width: '50vw',
    });

    dialogRef.afterClosed().subscribe(result => {
      this.onRefreshPage();
    });
  }

  async onExportCSV() {
    const resultBlob = await this.studentService.exportCSVFile();
    saveAs(resultBlob, 'Student export.csv');
  }
}
