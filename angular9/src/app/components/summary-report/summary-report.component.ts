import { Component, OnInit } from '@angular/core';
import { ChartDataSets, ChartOptions, ChartType } from 'chart.js';
import * as pluginDataLabels from 'chartjs-plugin-datalabels';
import { Label } from 'ng2-charts';
import {MarksService} from '../../services/marks.service';

@Component({
  selector: 'app-summary-report',
  templateUrl: './summary-report.component.html',
  styleUrls: ['./summary-report.component.scss']
})

export class SummaryReportComponent implements OnInit {
  public barChartOptions: ChartOptions = {
    responsive: true,
    scales: { xAxes: [{}], yAxes: [{}] },
    plugins: {
      datalabels: {
        anchor: 'end',
        align: 'end',
      }
    }
  };
  public barChartLabels: Label[] = ['Term One', 'Term Two', 'Term Three', 'Final Term'];
  public barChartType: ChartType = 'bar';
  public barChartLegend = true;
  public barChartPlugins = [pluginDataLabels];
  public barChartData: ChartDataSets[] = [];

  constructor(private marksService: MarksService) { }

  async ngOnInit() {
    const mathematics = await this.marksService.getMarks(1, '2190001');
    const english = await this.marksService.getMarks(3, '2190001');
    const accounting = await this.marksService.getMarks(5, '2190001');
    const physicalScience = await this.marksService.getMarks(7, '2190001');

    this.barChartData = [
      {data: [mathematics.task1, mathematics.task2, mathematics.task3, mathematics.task2], label: 'Mathematics'},
      {data: [english.task1, english.task2, english.task3, english.task2], label: 'English'},
      {data: [accounting.task1, accounting.task2, accounting.task3, accounting.task2], label: 'Accounting'},
      {data: [physicalScience.task1, physicalScience.task2, physicalScience.task3, physicalScience.task2], label: 'Physical Science'},
    ];
  }
}
