import { parse } from 'url';

export class IDmanipulations {
  public CalcAge(ID: string): number {
    let year = 0;

    if (parseInt(ID.substring(0, 1)) == 0) {
      year = parseInt(20 + ID.substring(0, 2));
    } else {
      year = parseInt(19 + ID.substring(0, 2));
    }

    return parseInt(new Date().getFullYear().toString()) - year;
  }

  public GetGender(ID: string): string {
    if (parseInt(ID.substring(6, 1)) > 5) {
        return 'Male';
     }
    else {
        return 'Female';
    }
  }
}
