import {Component, OnInit, OnDestroy} from '@angular/core';
import {Subject} from './models/Subject';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.scss']
})

export class AppComponent implements OnInit, OnDestroy {
    title = 'Start Smart';
    showFiller = false;
    //
    // subjects: Array<Subject>;

    constructor() {
    }

    ngOnInit() {
    }

    ngOnDestroy() {
    }
}
