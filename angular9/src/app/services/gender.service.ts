import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Gender } from '../models/Gender';

@Injectable({
  providedIn: 'root'
})

export class GenderService {

  constructor(private http: HttpClient){}

    private rootUrl = 'https://localhost:44347/api/gender';

    getGenders() {
      return this.http.get<Gender[]>(`${this.rootUrl}`).toPromise();
    }
}
