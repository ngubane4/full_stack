import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Subject} from '../models/Subject';
import {User} from '../models/User';

@Injectable({
  providedIn: 'root'
})
export class SubjectsService {
  constructor(private http: HttpClient) { }

  private rootUrl = 'https://localhost:44347/api';

  public getStudentSubjects(studentNumber){
    return this.http.get<Subject[]>(`${this.rootUrl}/Subjects/${studentNumber}`).toPromise();
  }
}
