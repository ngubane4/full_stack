import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Term} from '../models/Term';

@Injectable({
  providedIn: 'root'
})
export class MarksService {
  constructor(private http: HttpClient){}

  private rootUrl = 'https://localhost:44347/api/Marks';

  public getMarks(subId, username){
    return this.http.get<Term>(`${this.rootUrl}/${subId}/${username}`).toPromise();
  }
}
