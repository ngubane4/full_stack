import { Injectable } from '@angular/core';
import { Address } from '../models/Address';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class AddressService {

  constructor(private http: HttpClient) { }

  private rootUrl = 'https://localhost:44347/api';

  addAddress(address: Address) {
      return this.http.post<Address>(`${this.rootUrl}/address/`, address);
  }

  getAddress(studentNumber: number) {
      return this.http.get<Address>(`${this.rootUrl}/address/${studentNumber}`);
  }
  updateAddress(address: Address) {
      return this.http.put<Address>(`${this.rootUrl}/address/`, address);
  }
}
