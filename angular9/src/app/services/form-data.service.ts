import { SystemRole } from './../models/SystemRole';
import { Injectable } from '@angular/core';
import { Gender } from '../models/Gender';
import { MaritalStatus } from '../models/MaritalStatus';

@Injectable({
  providedIn: 'root'
})
export class FormDataService {

  private genderList: Gender[];
  private maritalStatusList: MaritalStatus[];
  private systemRolesList: SystemRole[];

  public set setGenderList(list: Gender[]) {
    this.genderList = list;
  }

  public get getGenderList(): Gender[] {
    return this.genderList;
  }

  public getGender(index: number): string {
    if (this.genderList !== undefined) {
      return this.genderList.find(x => x.id === index).type;
    }
  }

  public set setMaritalStatusList(list: MaritalStatus[]) {
    this.maritalStatusList = list;
  }

  public get getMaritalStatusList(): MaritalStatus[] {
    return this.maritalStatusList;
  }

  public set setSystemRoleList(list: SystemRole[]) {
    this.systemRolesList = list;
  }

  public get getSystemRolesList(): SystemRole[] {
    return this.systemRolesList;
  }
}
