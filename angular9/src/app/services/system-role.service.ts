import { HttpClient } from '@angular/common/http';
import { SystemRole } from './../models/SystemRole';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class SystemRoleService {

  constructor(private http: HttpClient) { }

  private rootUrl = 'https://localhost:44347/api';

  getSystemRoles() {
    return this.http.get<SystemRole[]>(`${this.rootUrl}/SystemRoles`).toPromise();
  }
}
