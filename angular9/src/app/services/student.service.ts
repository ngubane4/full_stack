import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { User } from '../models/User';

@Injectable({
  providedIn: 'root'
})
export class StudentService {

  constructor(private http: HttpClient) { }

  private rootUrl = 'https://localhost:44347/api';

  getStudents() {
    return this.http.get<User[]>(`${this.rootUrl}/student`).toPromise();
  }

  getStudentByStudentNumber(studentNumber: number) {
    return this.http.get<User>(`${this.rootUrl}/posts/?StudentNumber=${studentNumber}`);
  }

  addStudent(student: User) {
    return this.http.post<User>(`${this.rootUrl}/student`, student);
  }

  deleteStudent(studentNumber: number) {
    return this.http.delete(`${this.rootUrl}/student/${studentNumber}`);
  }

  updateStudent(student: User) {
    return this.http.put<User>(`${this.rootUrl}/student/`, student);
  }

  exportCSVFile() {
    return this.http.get(`${this.rootUrl}/student/ExportStudentsToCSV`,  {responseType: 'blob'}).toPromise();
  }
}
