import { TestBed } from '@angular/core/testing';

import { MarksService } from './marks.service';
import {HttpClientTestingModule} from '@angular/common/http/testing';

describe('MarksService', () => {

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [MarksService]
    });
  });

  it('should be created', () => {
    const service: MarksService = TestBed.get(MarksService);
    expect(service).toBeTruthy();
  });
});
