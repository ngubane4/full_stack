import { TestBed } from '@angular/core/testing';

import { SystemRoleService } from './system-role.service';
import {HttpClientTestingModule} from '@angular/common/http/testing';

describe('SystemRoleService', () => {
  beforeEach(() => TestBed.configureTestingModule({
    imports: [HttpClientTestingModule],
    providers: [SystemRoleService]
  }));

  it('should be created', () => {
    const service: SystemRoleService = TestBed.get(SystemRoleService);
    expect(service).toBeTruthy();
  });
});
