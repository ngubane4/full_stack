import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { MaritalStatus } from '../models/MaritalStatus';

@Injectable({
  providedIn: 'root'
})
export class MaritalStatusService {

  constructor(private http: HttpClient){}

  private rootUrl = 'https://localhost:44347/api/maritalStatus';

  getMaritalStatuses(){
      return this.http.get<MaritalStatus[]>(`${this.rootUrl}`).toPromise();
  }
}
