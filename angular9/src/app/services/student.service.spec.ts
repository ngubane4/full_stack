import { TestBed } from '@angular/core/testing';
import { StudentService } from './student.service';
import { HttpResponse } from '@angular/common/http';
import { User } from '../models/User';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { of } from 'rxjs';

describe('Testing Student Service', () => {
  let service: StudentService;
  const testStudentNumber = 12345;
  const testData: User[] = [
    {
      username: 2019123,
      name: 'Lindokuhle',
      surname: 'Ngubane',
      idNumber: '9951478531458',
      email: 'lindongubane4@gmail.com',
      genderId: 3,
      maritalStatusId: 3,
      password: 'test',
      roleId: 1
    },
    {
      username: 2019124,
      name: 'Senzo',
      surname: 'Ntuli',
      idNumber: '9051478531458',
      email: 'senzo@gmail.com',
      genderId: 3,
      maritalStatusId: 3,
      password: 'test',
      roleId: 1
    }
  ];

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [StudentService]
    });
  });

  describe('When calling getStudents', () => {
    let getStudentsSpy;

    beforeEach(() => {
      service = TestBed.inject(StudentService);
      getStudentsSpy = spyOn(service, 'getStudents').and.returnValue(Promise.resolve(testData));
    });

    it('Should be defined', () => {
      expect(service).toBeDefined();
    });

    it('should return list of students', async () => {
      const results = await service.getStudents();

      expect(results.length).toBe(2);
      expect(results).toEqual(testData);

      expect(getStudentsSpy.calls.count()).toBe(1);
    });
  });

  describe('Given student number', () => {
    let getStudentByStudentNumberSpy;
    const failResponse = new HttpResponse({ status: 404 });

    beforeEach(() => {
      service = TestBed.inject(StudentService);
      getStudentByStudentNumberSpy = spyOn(service, 'getStudentByStudentNumber').and.returnValue(of(testData[0]));
    });

    // describe('Given student number', () => {
    //   beforeEach(() => {

    //     service = TestBed.inject(StudentService);
    //     getStudentByStudentNumberSpy = spyOn(service, 'getStudentByStudentNumber').and.returnValue(of(failResponse));

    //     it('Should return 404', () => {
    //       service.getStudentByStudentNumber(null).subscribe(data =>
    //         expect(data).toEqual(failResponse));
    //     });
    //   });
    // });

    it('Shoube be defined', () => {
      expect(service).toBeDefined();
    });

    it('Should return one student', () => {
      service.getStudentByStudentNumber(testStudentNumber).subscribe(results => {
        expect(results).toEqual(testData[0]);
      });

      expect(getStudentByStudentNumberSpy).toHaveBeenCalledWith(testStudentNumber);
      expect(getStudentByStudentNumberSpy.calls.count()).toBe(1);
    });
  });

  describe('Given student when adding', () => {
    let addStudentSpy;

    beforeEach(() => {
      service = TestBed.inject(StudentService);
      addStudentSpy = spyOn(service, 'addStudent').and.returnValues(of(testData[0]));
    });

    it('Shoube be defined', () => {
      expect(service).toBeDefined();
    });

    it('Should create student then student record', () => {
      service.addStudent(testData[0]).subscribe(results => {
        expect(results).toEqual(testData[0]);
      });

      expect(addStudentSpy).toHaveBeenCalledWith(testData[0]);
      expect(addStudentSpy.calls.count()).toBe(1);
    });
  });

  describe('Given student number when deleting', () => {
    let deleteStudentSpy;

    beforeEach(() => {
      service = TestBed.inject(StudentService);
      deleteStudentSpy = spyOn(service, 'deleteStudent').and.returnValue(null);
    });

    it('Shoube be defined', () => {
      expect(service).toBeDefined();
    });

    it('Should delete student record', () => {
      service.deleteStudent(testStudentNumber);
      expect(deleteStudentSpy).toHaveBeenCalledWith(testStudentNumber);
    });
  });

  describe('when updating student', () => {
    let updateStudentSpy;

    beforeEach(() => {
      service = TestBed.inject(StudentService);
      updateStudentSpy = spyOn(service, 'updateStudent').and.returnValue(of(testData[0]));
    });

    it('Shoube be defined', () => {
      expect(service).toBeDefined();
    });

    it('Should update student record and return updated record', () => {
      service.updateStudent(testData[0]);
      expect(updateStudentSpy).toHaveBeenCalledWith(testData[0]);
    });
  });
});

