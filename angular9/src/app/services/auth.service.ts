import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { LogInModel } from '../models/LogInModel';
import * as jwt_decode from 'jwt-decode';

@Injectable({
    providedIn: 'root'
})

export class AuthService {
    private rootUrl = 'https://localhost:44347/api';

    constructor(private http: HttpClient) { }

    private getDecodedAccessToken(): any {
        try {
            return jwt_decode(this.getLocalToken());
        }
        catch (Error) {
            return null;
        }
    }

    logIn(loginModel: LogInModel) {
        return this.http.post<string>(`${this.rootUrl}/student/Auth/`, loginModel);
    }

    loggedIn() {
        return !!this.getLocalToken();
    }

    getUserRole() {
        const tokenInfo = this.getDecodedAccessToken();
        return tokenInfo.role;
    }

    getUserName(){
        const tokenInfo = this.getDecodedAccessToken();
        return tokenInfo.unique_name;
    }

    getLocalToken() {
        return sessionStorage.getItem('token');
    }
}
