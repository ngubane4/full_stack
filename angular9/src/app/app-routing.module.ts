import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './components/login/login.component';
import { StudentListingComponent } from './components/student-listing/student-listing.component';
import {SummaryReportComponent} from './components/summary-report/summary-report.component';
import {DashboardComponent} from './components/dashboard/dashboard.component';
import {NoticeBoardComponent} from './components/dashboard/notice-board/notice-board.component';
import {CalenderComponent} from './components/dashboard/calender/calender.component';
import {LeaderBoardComponent} from './components/dashboard/leader-board/leader-board.component';


const routes: Routes = [
  {
    path: '',
    redirectTo: 'login',
    pathMatch: 'full'
  },
  {
    path: 'login',
    component: LoginComponent
  },
  {
    path: 'student-listing',
    component: StudentListingComponent
  },
  {
    path: 'summary-report',
    component: SummaryReportComponent
  },
  {
    path: 'dashboard',
    component: DashboardComponent
  },
  {
    path: 'notice-board',
    component: NoticeBoardComponent
  },
  {
    path: 'calender',
    component: CalenderComponent
  },
  {
    path: 'leader-board',
    component: LeaderBoardComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { relativeLinkResolution: 'legacy' })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
