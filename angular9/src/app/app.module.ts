import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ReactiveFormsModule } from '@angular/forms';
import { FormsModule } from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';

import { LoginComponent } from './components/login/login.component';
import { ToolbarComponent } from './components/toolbar/toolbar.component';
import { StudentListingComponent } from './components/student-listing/student-listing.component';
import { DeleteUserComponent } from './components/delete-user/delete-user.component';
import { UpdateUserComponent } from './components/update-user/update-user.component';
import { AuthService } from './services/auth.service';
import { AuthGuard } from './routeGuard/auth.guard';
import { TokenInterceptorService } from './services/token-interceptor.service';
import { RegisterStudentComponent } from './components/register-student/register-student.component';

import {MatSelectModule} from '@angular/material/select';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatInputModule} from '@angular/material/input';
import {MatCardModule} from '@angular/material/card';
import {MatIconModule} from '@angular/material/icon';
import {MatButtonModule} from '@angular/material/button';
import {MatSidenavModule} from '@angular/material/sidenav';
import {MatToolbarModule} from '@angular/material/toolbar';
import {MatTableModule} from '@angular/material/table';
import {MatPaginatorModule} from '@angular/material/paginator';
import {MatDialogModule, MAT_DIALOG_DEFAULT_OPTIONS} from '@angular/material/dialog';
import {MatSnackBarModule} from '@angular/material/snack-bar';
import {MatTabsModule} from '@angular/material/tabs';
import {MatStepperModule} from '@angular/material/stepper';
import {MatListModule} from '@angular/material/list';
import {MatExpansionModule} from '@angular/material/expansion';
import {MatTreeModule} from '@angular/material/tree';
import {MatMenuModule} from '@angular/material/menu';
import { from } from 'rxjs';
import { SidenavComponent } from './components/sidenav/sidenav.component';
import { FlexLayoutModule } from '@angular/flex-layout';
import { ChartsModule } from 'ng2-charts';
import { SummaryReportComponent } from './components/summary-report/summary-report.component';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { CalenderComponent } from './components/dashboard/calender/calender.component';
import { LeaderBoardComponent } from './components/dashboard/leader-board/leader-board.component';
import { NoticeBoardComponent } from './components/dashboard/notice-board/notice-board.component';
import { PerformanceComponent } from './components/dashboard/performance/performance.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    ToolbarComponent,
    StudentListingComponent,
    UpdateUserComponent,
    DeleteUserComponent,
    RegisterStudentComponent,
    SidenavComponent,
    SummaryReportComponent,
    DashboardComponent,
    CalenderComponent,
    LeaderBoardComponent,
    NoticeBoardComponent,
    PerformanceComponent,
  ],
    entryComponents : [
      UpdateUserComponent,
      DeleteUserComponent,
      RegisterStudentComponent
    ],

  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    HttpClientModule,
    ReactiveFormsModule,
    FormsModule,
    MatFormFieldModule,
    MatInputModule,
    MatCardModule,
    MatIconModule,
    MatButtonModule,
    MatSidenavModule,
    MatToolbarModule,
    MatTableModule,
    MatPaginatorModule,
    MatDialogModule,
    MatSnackBarModule,
    MatTabsModule,
    MatSelectModule,
    MatStepperModule,
    MatExpansionModule,
    MatListModule,
    MatTreeModule,
    MatMenuModule,
    FlexLayoutModule,
    ChartsModule
  ],
  providers: [
    HttpClientModule,
    AuthGuard,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: TokenInterceptorService,
      multi: true
    },
    {
      provide: MAT_DIALOG_DEFAULT_OPTIONS,
      useValue: {hasBackdrop: false}
    },
    {provide: MAT_DIALOG_DEFAULT_OPTIONS, useValue: {hasBackdrop: true}}
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
