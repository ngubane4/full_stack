﻿namespace Models
{
    public class StudentSubjects
    {
        public int Id { get; set; }
        public int StuId { get; set; }
        public int SubId { get; set; }
    }
}
