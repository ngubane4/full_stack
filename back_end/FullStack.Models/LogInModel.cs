﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Models
{
    public class LogInModel
    {
        public int Username { get; set; }
        public string Password { get; set; }
    }
}
