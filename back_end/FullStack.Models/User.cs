﻿
namespace Models
{
    public class User
    {
        public int Username { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
        public string IdNumber { get; set; }
        public string Email { get; set; }
        public int GenderId { get; set; }
        public int MaritalStatusId { get; set; }
        public string Password { get; set; }
        public int RoleId { get; set; }
    }
}