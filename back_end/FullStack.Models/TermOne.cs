﻿namespace Models
{
    public class Term
    {
        public string TermName { get; set; }
        public int Id { get; set; }
        public int SubId { get; set; }
        public string Username { get; set; }
        public float Task1 { get; set; }
        public float Task2 { get; set; }
        public float Task3 { get; set; }
        public float Task4 { get; set; }
    }
}
