﻿namespace Models
{
    public class Stream
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}