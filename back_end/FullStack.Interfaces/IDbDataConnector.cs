﻿using System;
using System.Collections.Generic;

namespace Interfaces
{
    public interface IDbDataConnector
    {
        List<T> Read<T>(string proc, object param = null);
        bool Write(string proc, object param = null);
    }
}