﻿using Models;
using System.Collections.Generic;

namespace Interfaces.Repository
{
    public interface IMaritalStatusRepo
    {
        List<MaritalStatus> GetMaritalStatuses();
    }
}