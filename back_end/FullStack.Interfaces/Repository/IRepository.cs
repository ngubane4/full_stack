﻿using System.Collections.Generic;

namespace Interfaces.Repository
{
    public interface IRepository<T>
    {
        T Get(string proc, object param);
        T Get(string proc);
        List<T> GetAll(string proc);
        List<T> GetAll(string proc, object param);
    }
}