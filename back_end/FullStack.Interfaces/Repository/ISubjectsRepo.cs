﻿using Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Interfaces.Repository
{
    public interface ISubjectsRepo
    {
        public List<Subject> GetStudentSubjects(string studentNumber);
    }
}
