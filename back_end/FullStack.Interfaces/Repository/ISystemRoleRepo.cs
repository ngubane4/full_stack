﻿using Models;
using System.Collections.Generic;

namespace Interfaces.Repository
{
    public interface ISystemRoleRepo
    {
        List<SystemRole> GetSystemRoles();
        string GetUSerRole(int username);
        bool InserUserRole(int username, int roleId);
    }
}
