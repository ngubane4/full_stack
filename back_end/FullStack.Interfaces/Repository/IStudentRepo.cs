﻿using Models;
using System.Collections.Generic;

namespace Interfaces
{
    public interface IStudentRepo
    {
        User GetStudentByStudentNumber(int studentNumber);
        List<User> GetStudents();
        User AddStudent(User student);
        bool UpdateStudent(User student);
        bool DeleteStudent(int studentNumber);
        byte[] ExportStudentToCSV();
    }
}