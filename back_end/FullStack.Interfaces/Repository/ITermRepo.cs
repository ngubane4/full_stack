﻿using Models;

namespace Interfaces.Repository
{
    public interface ITermRepo
    {
        public Term getSubjectMarks(int subId, string username);
    }
}
