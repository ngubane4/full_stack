﻿using Models;
using System.Collections.Generic;

namespace Interfaces.Repository
{
    public interface IGenderRepo
    {
        List<Gender> GetGenders();
    }
}