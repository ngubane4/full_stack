﻿using Models;
using System.Collections.Generic;

namespace Interfaces.Repository
{
    public interface IRoleRepo
    {
        List<Role> GetAllRoles();

        bool AddUserRole(int roleId, int username);
    }
}
