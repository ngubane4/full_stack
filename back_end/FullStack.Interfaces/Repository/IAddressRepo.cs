﻿using Models;

namespace Interfaces
{
    public interface IAddressRepo
    {
        Address GetAddress(int addressId);

        Address AddAddress(Address address);

        bool UpdateAddress(Address address);
    }
}