﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Interfaces
{
    public interface IJwtAuthenticationManager
    {
        string Authenticate(int username, string password);
    }
}
