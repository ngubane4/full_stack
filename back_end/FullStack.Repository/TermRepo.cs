﻿using Interfaces;
using Interfaces.Repository;
using Models;
using System.Linq;

namespace Repository
{
    public class TermRepo : ITermRepo
    {
        private readonly IDbDataConnector connector;

        public TermRepo(IDbDataConnector connector )
        {
            this.connector = connector;
        }

        public Term getSubjectMarks(int subId, string username)
        {
            return connector.Read<Term>("spGetTermMarks", new { Username = username, subId = subId}).SingleOrDefault();
        }
    }
}
