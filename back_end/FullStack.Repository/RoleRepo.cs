﻿using Interfaces;
using Interfaces.Repository;
using Models;
using System.Collections.Generic;
using System.Linq;

namespace Repository
{
    public class RoleRepo : IRoleRepo
    {
        private readonly IDbDataConnector connector;

        public RoleRepo(IDbDataConnector connector)
        {
            this.connector = connector;
        }

        public List<Role> GetAllRoles()
        {
            return connector.Read<Role>("spGetAllRole").ToList();
        }

        public bool AddUserRole(int roleId, int username)
        {
            var param = new
            {
                RoleId = roleId,
                Username = username
            };

            return connector.Write("spAddUserRole", param);
        }
    }
}
