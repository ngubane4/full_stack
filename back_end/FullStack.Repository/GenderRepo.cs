﻿using Interfaces;
using Interfaces.Repository;
using Microsoft.AspNetCore.Http;
using Models;
using System.Collections.Generic;
using System.Linq;

namespace Repository
{
    public class GenderRepo : IGenderRepo
    {
        private IDbDataConnector connector;

        public GenderRepo(IDbDataConnector connector) 
        {
            this.connector = connector;
        }

        public List<Gender> GetGenders()
        {
            return connector.Read<Gender>("spGetAllGenders", null).ToList();
        }
    }
}