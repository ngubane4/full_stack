﻿using Interfaces;
using Interfaces.Repository;
using Models;
using System.Collections.Generic;
using System.Linq;

namespace Repository
{
    public class SystemRoleRepo : ISystemRoleRepo
    {
        private IDbDataConnector connector;

        public SystemRoleRepo(IDbDataConnector connector)
        {
            this.connector = connector;
        }

        public List<SystemRole> GetSystemRoles()
        {
            return connector.Read<SystemRole>("spGetAllSystemRoles", null).ToList();
        }

        public string GetUSerRole(int username)
        {
            return connector.Read<string>("spGetUserRole", new { Username = username })[0];
        }

        public bool InserUserRole(int username, int roleId)
        {
            return connector.Write("", new { userRole = roleId, username });
        }
    }
}
