﻿using Dapper;
using Interfaces;
using Repository.Helpers;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace Repository
{
    public class SqlDataConnector : IDbDataConnector
    {
        private IConnectionPool _connectionPool;

        public SqlDataConnector(IConnectionPool connectionPool)
        {
            _connectionPool = connectionPool;
        }

        public List<T> Read<T>(string proc, object param = null)
        {
            using (var connection = _connectionPool.CreateConnection())
            {
                return connection.Query<T>(proc, param, commandType: CommandType.StoredProcedure).ToList();
            }
        }
        
        public bool Write(string proc, object param = null)
        {
            using (var connection = _connectionPool.CreateConnection())
            {
                return connection.Execute(proc, param, commandType: CommandType.StoredProcedure) > 0;
            }
        }
    }
}