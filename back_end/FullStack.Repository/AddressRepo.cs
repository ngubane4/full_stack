﻿using Interfaces;
using Models;

namespace Repository
{
    public class AddressRepo : Repository<Address>, IAddressRepo
    {
        public AddressRepo(IDbDataConnector connector) : base(connector){}

        public Address AddAddress(Address address)
        {
            var param = new
            {
                address.City,
                address.Line1,
                address.Line2,
                address.Province,
                address.ZipCode,
                address.Username
            };

            if (Write("spInsertAddress", param))
            {
                return address;
            }

            return new Address();
        }

        public bool UpdateAddress(Address address)
        {
            return Write("spUpdateAddress", address);
        }

        public Address GetAddress(int username)
        {
            return Get("spGetAddress", new { Username = username });
        }
    }
}