﻿using Interfaces;
using Interfaces.Repository;
using Microsoft.AspNetCore.Http;
using System.Collections.Generic;
using System.Linq;

namespace Repository
{
    public abstract class Repository<T> : IRepository<T> 
    {
        private IDbDataConnector database;

        public Repository(IDbDataConnector connector)
        {
            database = connector;
        }
        
        public virtual T Get(string proc)
        {
            return Read(proc, null).SingleOrDefault();
        }
        
        public virtual T Get(string proc, object param)
        {
            return Read(proc, param).SingleOrDefault();
        }

        public virtual List<T> GetAll(string proc)
        {
            return Read(proc, null);
        }

        public virtual List<T> GetAll(string proc, object param)
        {
            return Read(proc, param);
        }

        private protected bool Write(string proc, object param)
        {
            return database.Write(proc, param);
        }

        private protected List<T> Read(string proc, object param)
        {
            return database.Read<T>(proc, param);
        }
    }
}