﻿using Interfaces;
using Interfaces.Repository;
using Models;
using System.Collections.Generic;
using System.Linq;

namespace Repository
{
    public class MaritalStatusRepo :  IMaritalStatusRepo
    {
        private readonly IDbDataConnector connector;

        public MaritalStatusRepo(IDbDataConnector connector)
        {
            this.connector = connector;
        }
        
        public List<MaritalStatus> GetMaritalStatuses()
        {
            return connector.Read<MaritalStatus>("spGetMaritalStatuses", null).ToList();
        }
    }
}