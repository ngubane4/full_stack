﻿using Interfaces;
using Interfaces.Repository;
using Models;
using System.Collections.Generic;
using System.Linq;

namespace Repository
{
    public class SubjectsRepo : ISubjectsRepo
    {
        private readonly IDbDataConnector connector;

        public SubjectsRepo(IDbDataConnector connector)
        {
            this.connector = connector;
        }

        public List<Subject> GetStudentSubjects(string studentNumber)
        {
            return connector.Read<Subject>("spGetStudebtSubjects", new { Username = studentNumber }).ToList();
        }
    }
}
