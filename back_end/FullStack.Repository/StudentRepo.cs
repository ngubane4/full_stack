﻿    using Interfaces;
using Interfaces.Repository;
using Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace Repository
{
    public class StudentRepo : IStudentRepo
    {
        private readonly IDbDataConnector connector;
        private readonly IGenderRepo genderRepo;
        private readonly IMaritalStatusRepo maritalStatusRepo;
        private readonly IRoleRepo roleRepo;

        public StudentRepo(IDbDataConnector connector, IGenderRepo genderRepo, IMaritalStatusRepo maritalStatusRepo, IRoleRepo roleRepo)
        {
            this.connector = connector;
            this.genderRepo = genderRepo;
            this.maritalStatusRepo = maritalStatusRepo;
            this.roleRepo = roleRepo;
        }

        //to be removed
        private int GenerateStudentNumber()
        {
            var currentDate = DateTime.Now.Year.ToString().ToCharArray();
            var lastStudent = connector.Read<User>("spGelLastUserTableIndex", null).SingleOrDefault().Username.ToString();
            var dateValue = $"{currentDate[0]}{currentDate[2]}{currentDate[3]}";

            if ((lastStudent.Length == 1) || !dateValue.Equals(lastStudent.Substring(0, 3)))
            {
                return int.Parse($"{dateValue}0000") + 1;
            }

            var studentNumber = $"{dateValue}{lastStudent.Substring(3)}";

            return int.Parse(studentNumber) + 1;
        }

        public User GetStudentByStudentNumber(int StudentNumber)
        {
            return connector.Read<User>("spSelectUser", new { Username = StudentNumber }).SingleOrDefault();
        }

        public User AddStudent(User student)
        {
            student.Username = GenerateStudentNumber();
            var param = new
            {
                student.Username,
                student.Name,
                student.Surname,
                student.IdNumber,
                student.Email,
                student.GenderId,
                student.MaritalStatusId,
            };

            if (connector.Write("spInsertUser", param))
            {
                roleRepo.AddUserRole(student.RoleId, student.Username);

                return student;
            }

            return null;
        }

        public List<User> GetStudents()
        {
            return connector.Read<User>("spSelectAllStudents", null).ToList();
        }

        public bool DeleteStudent(int StudentNumber)
        {
            var param = new { Username = StudentNumber };

            return connector.Write("spDeleteUser", param);
        }

        public bool UpdateStudent(User student)
        {
            var param = new
            {
                student.Username,
                student.IdNumber,
                student.Name,
                student.Surname,
                student.Email,
                student.MaritalStatusId,
                student.GenderId
            };

            return connector.Write("spUpdateStudent", param);
        }

        public byte[] ExportStudentToCSV()
        {
            var file = new StringBuilder();
            var allStudents = GetStudents();
            var genderTypes = genderRepo.GetGenders();
            var maritalStatuses = maritalStatusRepo.GetMaritalStatuses();

            foreach (var user in allStudents)
            {
                var genderType = genderTypes.Find(type => type.Id == user.GenderId).Type;
                var maritalStatus = maritalStatuses.Find(status => status.Id == user.MaritalStatusId).status;
                file.AppendLine(string.Format($"{user.Username},{user.Name},{user.Surname},{user.IdNumber},{user.Email},{genderType},{maritalStatus}"));
            }

            return Encoding.ASCII.GetBytes(file.ToString());
        }
    }
}
