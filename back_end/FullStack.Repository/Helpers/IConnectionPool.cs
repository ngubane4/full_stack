﻿using System;
using System.Data;

namespace Repository.Helpers
{
    public interface IConnectionPool : IDisposable
    {
        void CloseConnection(IDbConnection connection);
        IDbConnection CreateConnection();
    }
}