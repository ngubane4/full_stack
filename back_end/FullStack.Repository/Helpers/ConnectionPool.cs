﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;

namespace Repository.Helpers
{
    public class ConnectionPool : IConnectionPool
    {
        private string _connectionString;
        private bool disposed;

        private Queue<IDbConnection> _availableConnetions = new Queue<IDbConnection>();
        private List<IDbConnection> _inUseConnections = new List<IDbConnection>();

        public ConnectionPool(string connectionString)
        {
            _connectionString = connectionString;
        }

        ~ConnectionPool()
        {
            Dispose(false);
        }

        public void Dispose()
        {
            Dispose(false);
            GC.SuppressFinalize(this);
        }

        public void Dispose(bool disposing)
        {
            if (disposed)
            {
                return;
            }

            if (disposing)
            {
                _availableConnetions.Enqueue(_inUseConnections[0]);
                _inUseConnections.Remove(_inUseConnections[0]);
            }

            disposed = true;
        }

        public IDbConnection CreateConnection()
        {
            IDbConnection connection;

            if (_availableConnetions.Count != 0)
            {
                connection = _availableConnetions.Peek();
                _inUseConnections.Add(connection);
                _availableConnetions.Dequeue();
                return connection;
            }

            connection = new SqlConnection(_connectionString);
            _inUseConnections.Add(connection);
            return connection;
        }

        public void CloseConnection(IDbConnection connection)
        {
            _availableConnetions.Enqueue(connection);
            _inUseConnections.Remove(connection);
        }
    }
}
