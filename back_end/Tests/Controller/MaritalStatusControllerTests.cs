﻿using API.Controllers;
using Interfaces.Repository;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Models;
using NSubstitute;
using NUnit.Framework;
using System.Collections.Generic;

namespace Tests.Controller
{
    [TestFixture]
    [Category("Unit Tests")]
    public class MaritalStatusControllerTests
    {
        IMaritalStatusRepo maritalStatusRepo;
        MaritalStatusController maritalStatusController;
        List<MaritalStatus> maritalStatusList;

        [SetUp]
        public void SetUp()
        {
            maritalStatusRepo = Substitute.For<IMaritalStatusRepo>();
            maritalStatusController = new MaritalStatusController(maritalStatusRepo);

            maritalStatusList = new List<MaritalStatus>()
            {
                new MaritalStatus{ Id = 1, status = "Single"},
                new MaritalStatus{ Id = 1, status = "Marid"},
                new MaritalStatus{ Id = 1, status = "Devoced"},
                new MaritalStatus{ Id = 1, status = "Other"},
            };
        }

        [Test]
        public void Should_Return_MarutalStatus_List()
        {
            maritalStatusRepo.GetMaritalStatuses().Returns(maritalStatusList);

            var results = maritalStatusController.GetMaritalStatuses() as ObjectResult;

            Assert.Multiple(() =>
            {
                Assert.AreEqual(4, ((List<MaritalStatus>)results.Value).Count);
                Assert.AreEqual(StatusCodes.Status200OK, results.StatusCode);
            });
        }

        [Test]
        public void Should_Return_NotFound()
        {
            maritalStatusRepo.GetMaritalStatuses().Returns((List<MaritalStatus>)null);

            var results = maritalStatusController.GetMaritalStatuses() as ObjectResult;

            Assert.Multiple(() => 
            {
                Assert.IsNull(((List<MaritalStatus>)results.Value));
                Assert.AreEqual(StatusCodes.Status404NotFound, results.StatusCode);
            });
        }
    }
}
