﻿using API.Controllers;
using Interfaces;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Models;
using NSubstitute;
using NUnit.Framework;

namespace Tests.Controller
{
    [TestFixture]
    [Category("Unit Tests")]
    public class AddressControllerTests
    {
        private IAddressRepo addressRepo;
        private AddressController addressController;
        private Address address;

        [SetUp]
        public void SetUp()
        {
            var dbConn = Substitute.For<IDbDataConnector>();
            addressRepo = Substitute.For<IAddressRepo>();
            addressController = new AddressController(addressRepo);

            address = new Address()
            {
                City = "City",
                Line1 = "line1",
                Line2 = "line2",
                Province = "province",
                Username = 1234567,
                ZipCode = 4001
            };
        }

        [Test]
        public void Given_Address_Should_True()
        {
            addressRepo.AddAddress(Arg.Any<Address>()).Returns(address);

            var results = addressController.AddAddress(address) as ObjectResult;

            Assert.Multiple(() =>
            {
                Assert.AreEqual(address, results.Value);
                Assert.AreEqual(StatusCodes.Status201Created, results.StatusCode);
                Assert.IsInstanceOf<Address>(results.Value);
            });
        }

        [Test]
        public void Given_Address_Should_Update_And_Return_True()
        {
            addressRepo.UpdateAddress(Arg.Any<Address>()).Returns(true);

            var results = addressController.UpdateAddress(address) as ObjectResult;

            Assert.Multiple(() =>
            {
                Assert.IsTrue((bool)results.Value);
                Assert.AreEqual(StatusCodes.Status200OK, results.StatusCode);
            });
        }

        [Test]
        public void Given_Student_Number_Should_Return_Student_Address()
        {
            addressRepo.GetAddress(Arg.Any<int>()).Returns(address);

            var results = addressController.GetAddress(2190001) as ObjectResult;

            Assert.Multiple(() =>
            {
                Assert.AreEqual(address, results.Value);
                Assert.AreEqual(StatusCodes.Status200OK, results.StatusCode);
            });
        }

        //Negative 
        [Test]
        public void Given_Address_Should_When_Adding_Should_Return_False()
        {
            addressRepo.AddAddress(Arg.Any<Address>()).Returns((Address)null);

            var results = addressController.AddAddress(address) as ObjectResult;

            Assert.Multiple(() =>
            {
                Assert.AreNotEqual(address, results.Value);
                Assert.AreEqual(StatusCodes.Status404NotFound, results.StatusCode);
                Assert.IsNotInstanceOf<Address>(results.Value);
            });
        }

        [Test]
        public void Given_Address_When_Updating_Shlould_Return_False()
        {
            addressRepo.UpdateAddress(Arg.Any<Address>()).Returns(false);

            var results = addressController.UpdateAddress(address) as ObjectResult;

            Assert.Multiple(() =>
            {
                Assert.IsFalse((bool)results.Value);
                Assert.AreEqual(StatusCodes.Status304NotModified, results.StatusCode);
            });
        }

        [Test]
        public void Given_Invalid_Student_Number_Should_Return_NotFound()
        {
            addressRepo.GetAddress(Arg.Any<int>()).Returns((Address)null);

            var results = addressController.GetAddress(2190001) as ObjectResult;

            Assert.AreEqual(StatusCodes.Status404NotFound, results.StatusCode);
        }
    }
}
