﻿using API;
using Dapper;
using Microsoft.AspNetCore.Mvc.Testing;
using Models;
using Newtonsoft.Json;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace Tests.Controller
{
    [Category("Intergration Test")]
    [TestFixture]
    class GenderControllerTests
    {
        protected HttpClient httpClient;
        
        [SetUp]
        public void SetUp()
        {
            var appFac = new WebApplicationFactory<Startup>();
            httpClient = appFac.CreateClient();
            //httpClient.PostAsync
        }

        //[TearDown]
        public void TearDown()
        {
            using var connection = new SqlConnection("Data Source=localhost;Initial Catalog=StartSmart;Integrated Security=True;");
            var sql = "delete from Address; Delete from Students where StudentNumber != 2190001 and StudentNumber != 2190002; ";
            connection.Execute(sql);
        }

        [Test]
        public async Task Should_Return_All_GenderTypes()
        {
            var respose = await httpClient.GetAsync("https://localhost:44347/api/Gender");
            var results = await respose.Content.ReadAsStringAsync();

            Assert.Multiple(() => 
            {
                Assert.AreEqual(HttpStatusCode.OK, respose.StatusCode);
                Assert.IsInstanceOf<List<Gender>>(JsonConvert.DeserializeObject<List<Gender>>(results));
                Assert.AreEqual(4, JsonConvert.DeserializeObject<List<Gender>>(results).Count);
                Assert.AreEqual("Male", JsonConvert.DeserializeObject<List<Gender>>(results)[0].Type);
            });
        }
    }
}
