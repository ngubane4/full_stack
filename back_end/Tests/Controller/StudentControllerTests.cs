﻿using API;
using Dapper;
using Microsoft.AspNetCore.Mvc.Testing;
using Models;
using Newtonsoft.Json;
using NUnit.Framework;
using System.Data.SqlClient;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace Tests.Controller
{
    [Category("Intergration Test")]
    [TestFixture]
    class StudentControllerTests
    {
        protected HttpClient httpClient;
        private User studentTestData;
        private async Task<User> GetTestStudent()
        {
            var data = new StringContent(JsonConvert.SerializeObject(studentTestData), Encoding.UTF8, "application/json");
            var testResponse = await httpClient.PostAsync("https://localhost:44347/api/student", data);
            var results = await testResponse.Content.ReadAsStringAsync();
            return JsonConvert.DeserializeObject<User>(results);
        }

        [SetUp]
        public void SetUp()
        {
            var appFac = new WebApplicationFactory<Startup>();
            httpClient = appFac.CreateClient();
            //httpClient.PostAsync

            studentTestData = new User
            {
                Email = "lindo@ngubane.com",
                GenderId = 1,
                IdNumber = "12345",
                MaritalStatusId = 1,
                Name = "Lindo",
                Password = "StartSmart",
                RoleId = 1,
                Surname = "Ngubane",
                Username = 220001
            };
        }

        //[TearDown]
        public void TearDown()
        {
            using var connection = new SqlConnection("Data Source=localhost;Initial Catalog=StartSmart;Integrated Security=True;");
            var sql = "delete from Address; Delete from Students where StudentNumber != 2190001 and StudentNumber != 2190002; ";
            connection.Execute(sql);
        }

        //[Test]
        //public async Task Should_Return_All_Students()
        //{
        //    var respose = await httpClient.GetAsync("https://localhost:44347/api/student");
        //    var results = await respose.Content.ReadAsStringAsync();

        //    Assert.AreEqual(HttpStatusCode.OK, respose.StatusCode);
        //    Assert.IsInstanceOf<List<User>>(JsonConvert.DeserializeObject<List<User>>(results));
        //    Assert.AreEqual(2, JsonConvert.DeserializeObject<List<User>>(results).Count);
        //}
    }
}
