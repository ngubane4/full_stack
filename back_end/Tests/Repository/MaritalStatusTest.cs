﻿using Interfaces;
using Models;
using NSubstitute;
using NUnit.Framework;
using Repository;
using System.Collections.Generic;

namespace Tests.Repository
{
    [TestFixture]
    [Category("Unit Tests")]
    class MaritalStatusTest
    {
        private IDbDataConnector connecter;
        private List<MaritalStatus> maritalStatusList = new List<MaritalStatus>
        {
            new MaritalStatus{Id = 1, status = "Single"},
            new MaritalStatus{Id = 2, status = "Marid"}
        };

        [Test]
        public void For_GetMaritalStatuses_ShouldReturnListOfMaritalStatusesTypes()
        {
            connecter = Substitute.For<IDbDataConnector>();
            connecter.Read<MaritalStatus>(Arg.Any<string>(), Arg.Any<object>()).Returns(maritalStatusList);
            var maritalStatusRepo = new MaritalStatusRepo(connecter);

            var results = maritalStatusRepo.GetMaritalStatuses();

            Assert.AreEqual(2, results.Count);
        }
    }
}
