﻿using Interfaces;
using Models;
using NSubstitute;
using NUnit.Framework;
using Repository;
using System.Collections.Generic;

namespace Tests.Repository
{
    [TestFixture]
    [Category("Unit Tests")]
    class GenderTest
    {
        private IDbDataConnector connecter;
        private List<Gender> genderList = new List<Gender> 
        {
            new Gender{Id = 1, Type = "Male"},
            new Gender{Id = 2, Type = "Female"}
        };

        [Test]
        public void For_GetAllGendes_ShouldReturnListOfGenderTypes()
        {
            connecter =  Substitute.For<IDbDataConnector>();
            connecter.Read<Gender>(Arg.Any<string>(), Arg.Any<object>()).Returns(genderList);
            var genderRepo = new GenderRepo(connecter);

            var results = genderRepo.GetGenders();

            Assert.AreEqual(2, results.Count);
        }
    }
}
