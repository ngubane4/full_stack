﻿using Interfaces;
using Interfaces.Repository;
using Models;
using NSubstitute;
using NUnit.Framework;
using Repository;
using System.Collections.Generic;

namespace Tests.Repository
{
    [TestFixture]
    [Category("Unit Tests")]
    class StudentRepoTests
    {
        private IDbDataConnector connector;
        private IStudentRepo studentRepo;
        private IGenderRepo genderRepo;
        private IMaritalStatusRepo maritalStatusRepo;
        private IRoleRepo roleRepo;

        [SetUp]
        public void SetUp()
        {
            connector = Substitute.For<IDbDataConnector>();
            genderRepo = Substitute.For<IGenderRepo>();
            maritalStatusRepo = Substitute.For<IMaritalStatusRepo>();
            roleRepo = Substitute.For<IRoleRepo>();
            studentRepo = new StudentRepo(connector, genderRepo, maritalStatusRepo, roleRepo);
        }

        [Test]
        public void GetStudentByStudentNumber_ShouldReturnStudent()
        {
            connector.Read<User>(Arg.Any<string>(), Arg.Any<object>()).Returns(new List<User> { new User { Username = 212321 } });

            var results = studentRepo.GetStudentByStudentNumber(212321);

            Assert.IsInstanceOf<User>(results);
        }

        [Test]
        public void AddStudent_GivenStudentShouldReturnNewStudent()
        {
            connector.Write(Arg.Any<string>(), Arg.Any<object>()).Returns(true);
            connector.Read<User>(Arg.Any<string>(), Arg.Any<object>()).Returns(new List<User>() { new User { Username = 2200004 } });

            var results = studentRepo.AddStudent(new User { RoleId = 1 });

            Assert.AreEqual(2220001, results.Username);
            Assert.IsInstanceOf<User>(results);
        }

        [Test]
        public void DeleteStudent_GivenStudentNumber_ShouldReturnTrue()
        {
            connector.Write(Arg.Any<string>(), Arg.Any<object>()).Returns(true);

            var results = studentRepo.DeleteStudent(12345678);

            Assert.IsTrue(results);
        }

        [Test]
        public void UpdateStudent_GivenUserShouldReturnTrue()
        {
            connector.Write(Arg.Any<string>(), Arg.Any<object>()).Returns(true);

            var results = studentRepo.UpdateStudent(new User { });

            Assert.IsTrue(results);
        }

        [Test]
        public void AddStudent_GivenInvalidStudent_ShouldReturnNull()
        {
            connector.Write(Arg.Any<string>(), Arg.Any<object>()).Returns(false);
            connector.Read<User>(Arg.Any<string>(), Arg.Any<object>()).Returns(new List<User>() { new User {} });

            var results = studentRepo.AddStudent(new User { MaritalStatusId = 4, GenderId = 4 });

            Assert.IsNull(results);
        }

        [Test]
        public void UpdateStudent_GivenInvalidStudentNumber_ShouldReturnFalse()
        {
            connector.Write(Arg.Any<string>(), Arg.Any<object>()).Returns(false);

            var results = studentRepo.UpdateStudent(new User{ IdNumber = "123456789", });

            Assert.IsFalse(results);
        }
    }
}