﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using Interfaces.Repository;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class SubjectsController : ControllerBase
    {
        private readonly ISubjectsRepo subjectsRepo;

        public SubjectsController(ISubjectsRepo subjectsRepo)
        {
            this.subjectsRepo = subjectsRepo;
        }

        [HttpGet("{studentNumber}")]
        public IActionResult GetStudentSubjects(string studentNumber)
        {
            return Ok(subjectsRepo.GetStudentSubjects(studentNumber));
        }
    }
}
