﻿using Interfaces;
using Microsoft.AspNetCore.Mvc;
using Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;

namespace API.Controllers
{
   // [Authorize]
    [ApiController]
    [Route("api/[controller]")]
    [Consumes("application/json")]
    [Produces("application/json")]
    public class AddressController : ControllerBase
    {
        private readonly IAddressRepo _addressRepo;
        public AddressController(IAddressRepo addressRepo)
        {
            _addressRepo = addressRepo;
        }

        /// <summary>
        /// Add New Student Address
        /// </summary>
        /// <param name="address">This is the student address object</param>
        /// <returns>Newly Created student address</returns>
        // <response code="201">Created</response>
        [HttpPost]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ApiConventionMethod(typeof(DefaultApiConventions), nameof(DefaultApiConventions.Post))]
        public IActionResult AddAddress([FromBody] Address address)
        {
            var results = _addressRepo.AddAddress(address);

            if (results != null)
            {
                return Created("new url", results);
            }

            return NotFound("Student number not found");
        }
        
        /// <summary>
        /// Get student address
        /// </summary>
        /// <param name="studentNumber">Student Number</param>
        /// <returns>student address that match the given student number</returns> 
        [HttpGet("{studentNumber}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ApiConventionMethod(typeof(DefaultApiConventions), nameof(DefaultApiConventions.Get))]
        public IActionResult GetAddress(int studentNumber)
        {
            var results = _addressRepo.GetAddress(studentNumber);

            if(results != null)
            {
                return Ok(results);
            }

            return NotFound("Address not found");
        }

        /// <summary>
        /// Update student address
        /// </summary>
        /// <param name="address">New Address</param>
        /// <returns>The newly updated student address</returns>
        [HttpPut]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status304NotModified)]
        public IActionResult UpdateAddress([FromBody] Address address)
        {
            var results = _addressRepo.UpdateAddress(address);

            if(results)
            {
                return Ok(results);
            }

            return StatusCode(StatusCodes.Status304NotModified, results);
        }
    }
}