﻿using Interfaces.Repository;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Serilog;

namespace API.Controllers
{
    //[Authorize]
    [ApiController]
    [Route("api/[controller]")]
    [Produces("application/json")]
    public class GenderController : ControllerBase
    {
        private readonly IGenderRepo _genderRepo;
        public GenderController(IGenderRepo genderRepo)
        {
            _genderRepo = genderRepo;
        }

        /// <summary>
        /// Get all gender options 
        /// </summary>
        /// <returns>Returns all gender options</returns>
        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public IActionResult GetGenders()
        {
            var results = _genderRepo.GetGenders();

            if (results != null)
            {
                Log.Information("Data Found {results}", results);

                return Ok(results);
            }

            return NotFound(results);
        }
    }
}