﻿using Interfaces.Repository;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ModelBinding.Validation;
using System;

namespace API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class MarksController : ControllerBase
    {
        private ITermRepo termRepo;
        public MarksController(ITermRepo termRepo)
        {
            this.termRepo = termRepo;
        }

        [HttpGet("{subId}/{username}")]
        public IActionResult GetTermMarks(int subId, string username)
        {
            return Ok(termRepo.getSubjectMarks(subId, username));
        }
    }
}
