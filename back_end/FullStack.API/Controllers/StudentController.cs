﻿using Interfaces;
using Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;

namespace API.Controllers
{
    [Authorize]
    [ApiController]
    [Route("api/[controller]")]
    [Produces("application/json")]
    public class StudentController : ControllerBase
    {
        private readonly IStudentRepo _studentRepo;
        private readonly IJwtAuthenticationManager jwtAuthenticationManager;

        public StudentController(IStudentRepo studentRepo, IJwtAuthenticationManager jwtAuthenticationManager)
        {
            _studentRepo = studentRepo;
            this.jwtAuthenticationManager = jwtAuthenticationManager;
        }

        /// <summary>
        /// Exports all students to a CSV file
        /// </summary>
        /// <returns>Returns csv file</returns>
        [HttpGet("ExportStudentsToCSV")]
        public FileResult DownloadCSVFile()
        {
            var fileBytes = _studentRepo.ExportStudentToCSV();

            return File(fileBytes, "text/csv", "Lindo.csv");
        }

        [AllowAnonymous]
        [HttpPost("Auth")]
        public IActionResult Auth([FromBody] LogInModel logInModel)
        {
            var token = jwtAuthenticationManager.Authenticate(logInModel.Username, logInModel.Password);

            if(token != null)
            {
                return Ok(token);
            }

            return Unauthorized("Wrong student number or password");
        }

        /// <summary>
        /// Insert new student
        /// </summary>
        /// <returns>Returns newly creates student</returns>
        [HttpPost]
        [ProducesResponseType(StatusCodes.Status201Created)]
        public IActionResult Students([FromBody] User student)
        {
            var results = _studentRepo.AddStudent(student);
                
            if(results != null)
            {
                return Created("new url", results);
            }
       
            return BadRequest(results);
        }

        /// <summary>
        /// Get list of students
        /// </summary>
        /// <returns>Returns list of students</returns>
        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public IActionResult Students()
        {
            return Ok(_studentRepo.GetStudents());
        }

        /// <summary>
        /// Get Student by Student number
        /// </summary>
        /// <param name="studentNumber">Student Number</param>
        /// <returns>Returns student</returns>
        [HttpGet("{studentNumber}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public IActionResult GetStudentByStudentNumber(int studentNumber)
        {
            var results = _studentRepo.GetStudentByStudentNumber(studentNumber);

            if (results == null)
            {
                return NotFound(results);
            }

            return Ok(results);
        }

        /// <summary>
        /// Update Student information
        /// </summary>
        /// <param name="student"></param>
        [HttpPut]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status304NotModified)]
        public IActionResult Student([FromBody] User student)
        {
            var results = _studentRepo.UpdateStudent(student);

            if (results)
            {
                return Ok(results);
            }

            return StatusCode(StatusCodes.Status304NotModified, results);
        }

        /// <summary>
        /// Delete student by student number
        /// </summary>
        /// <param name="studentNumber"></param>
        /// <returns></returns>
        [HttpDelete("{studentNumber}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public IActionResult Student(int studentNumber)
        {
            var results = _studentRepo.DeleteStudent(studentNumber);

            if (results)
            {
                return Ok(results);
            }

            return NotFound(results);
        }
    }
}