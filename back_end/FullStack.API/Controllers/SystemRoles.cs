﻿using Interfaces.Repository;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Serilog;

namespace API.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    [Produces("application/json")]
    public class SystemRoles : ControllerBase
    {
        private readonly ISystemRoleRepo systemRoleRepo;

        public SystemRoles(ISystemRoleRepo systemRoleRepo)
        {
            this.systemRoleRepo = systemRoleRepo;
        }

        /// <summary>
        /// Get all system roles options 
        /// </summary>
        /// <returns>Returns all role options</returns>
        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public IActionResult GetAllSystemRoles()
        {
            var results = systemRoleRepo.GetSystemRoles();

            if (results != null)
            {
                Log.Information("Data Found {results}", results);

                return Ok(results);
            }

            return NotFound(results);
        }
    }
}
