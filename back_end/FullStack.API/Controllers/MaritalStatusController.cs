﻿using Interfaces.Repository;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Repository.CustomException;
using System.Net;

namespace API.Controllers
{
    //[Authorize]
    [ApiController]
    [Route("api/[controller]")]
    [Produces("application/json")]
    public class MaritalStatusController : ControllerBase
    {
        private readonly IMaritalStatusRepo _maritalStatusRepo;

        public MaritalStatusController(IMaritalStatusRepo maritalStatus)
        {
            _maritalStatusRepo = maritalStatus;
        }
        /// <summary>
        /// Get all marital status options 
        /// </summary>
        /// <returns>Returns all marital status options</returns>
        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public IActionResult GetMaritalStatuses()
        {
            var results = _maritalStatusRepo.GetMaritalStatuses();

            //throw new System.Exception("Test data");
            
            
            // have spedif error class; have this at the uper level only/
           // throw new CustomHttpException(HttpStatusCode.BadRequest, "Test data");


            if (results != null)
            {
                return Ok(results);
            }
                
            return NotFound(results);
        }
    }
}