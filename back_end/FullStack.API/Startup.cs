using Autofac;
using Autofac.Extensions.DependencyInjection;
using Interfaces;
using Repository;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Interfaces.Repository;
using API.CustomExceptionMiddleware;
using Serilog;
using Microsoft.OpenApi.Models;
using System.Reflection;
using System.IO;
using System;
using Microsoft.AspNetCore.Http;
using API.Helpers;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.IdentityModel.Tokens;
using System.Text;
using Repository.Helpers;

[assembly: ApiConventionType(typeof(DefaultApiConventions))]
namespace API
{
    public class Startup
    {
        public IContainer ApplicationContainer;
        public IConfiguration Configuration { get; }

        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public static IConnectionPool CreatePool(IServiceProvider provider)
        {
            var config = provider.GetService<IConfiguration>();
            var connectionString = config.GetSection("ConnectionString").Value;
            return new ConnectionPool(connectionString);
        }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddSingleton(Configuration);
            services.AddScoped(CreatePool);
            services.AddScoped<IDbDataConnector, SqlDataConnector>();

            services.AddScoped<IStudentRepo, StudentRepo>();
            services.AddScoped<IAddressRepo, AddressRepo>();
            services.AddScoped<IGenderRepo, GenderRepo>();
            services.AddScoped<IMaritalStatusRepo, MaritalStatusRepo>();
            services.AddScoped<ISystemRoleRepo, SystemRoleRepo>();
            services.AddScoped<ITermRepo, TermRepo>();
            services.AddScoped<ISubjectsRepo, SubjectsRepo>();
            services.AddScoped<IRoleRepo, RoleRepo>();

            var key = "This is my fullstack super strong key. dont hack me";
            services.AddScoped<IJwtAuthenticationManager>((provider) => new JwtAuthenticationManager(key, provider.GetService<IStudentRepo>(), provider.GetService<ISystemRoleRepo>()));
            services.AddAuthentication(x =>
            {
                x.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                x.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;

            }).AddJwtBearer(x =>
            {
                x.RequireHttpsMetadata = false;
                x.SaveToken = true;
                x.TokenValidationParameters = new TokenValidationParameters
                {
                    ValidateIssuerSigningKey = true,
                    IssuerSigningKey = new SymmetricSecurityKey(Encoding.ASCII.GetBytes(key)),
                    ValidateIssuer = false,
                    ValidateAudience = false
                };
            });

            services.AddCors(options =>
            {
                options.AddPolicy("CorsPolicy",
                    builder => builder.AllowAnyOrigin()
                    .AllowAnyMethod()
                    .AllowAnyHeader());
            });
            services.AddControllers();

            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo
                {
                    Title = "Student System API",
                    Version = "v1"
                });

                var xmlCommentsFile = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";
                var xmlCommentsFullPath = Path.Combine(AppContext.BaseDirectory, xmlCommentsFile);
                c.IncludeXmlComments(xmlCommentsFullPath);
            });

            services.AddMvc(setupAction =>
            {
                setupAction.ReturnHttpNotAcceptable = true;

                setupAction.Filters.Add(new ProducesResponseTypeAttribute(StatusCodes.Status400BadRequest));
                setupAction.Filters.Add(new ProducesResponseTypeAttribute(StatusCodes.Status404NotFound));
                setupAction.Filters.Add(new ProducesResponseTypeAttribute(StatusCodes.Status406NotAcceptable));
                setupAction.Filters.Add(new ProducesResponseTypeAttribute(StatusCodes.Status401Unauthorized));
                setupAction.Filters.Add(new ProducesResponseTypeAttribute(StatusCodes.Status500InternalServerError));

            }).SetCompatibilityVersion(CompatibilityVersion.Latest);

            var builder = new ContainerBuilder();
            builder.Populate(services);

            ApplicationContainer = builder.Build();
        }

        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseSwagger();
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "Student System API V1");
            });

            app.UseMiddleware(typeof(ErrorHandlingMiddleware));

            app.UseCors("CorsPolicy");
            app.UseSerilogRequestLogging();
            app.UseHttpsRedirection();
            app.UseRouting();

            app.UseAuthentication();
            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}