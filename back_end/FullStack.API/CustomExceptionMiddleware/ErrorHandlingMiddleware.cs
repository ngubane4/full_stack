﻿using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;
using Repository.CustomException;
using Serilog;
using System;
using System.Net;
using System.Threading.Tasks;

namespace API.CustomExceptionMiddleware
{
    public class ErrorHandlingMiddleware
    {
        private readonly RequestDelegate next;
        public ErrorHandlingMiddleware(RequestDelegate next)
        {
            this.next = next;
        }

        public async Task Invoke(HttpContext context)
        {
            try
            {
                await next(context);
            }
            catch (CustomHttpException ex)
            {
                context.Response.StatusCode = (int)ex.StatusCode;
                await HandleExceptionAsync(context, ex);
            }
            catch (Exception ex)
            {
                await HandleExceptionAsync(context, ex);
            }
        }

        private static Task HandleExceptionAsync(HttpContext context, Exception ex)
        {
            context.Response.ContentType = "application/json";

            if (ex is CustomHttpException)
            {
                var result = JsonConvert.SerializeObject(new { context.Response.StatusCode, errorMessage = ex.Message });
                
                Log.Error("Custom Exception Message: {Message} StatusCode: {StatusCode} StackeTrace: {StackTrace}", ex.Message,  context.Response.StatusCode, ex.StackTrace);

                return context.Response.WriteAsync(result);
            }

            context.Response.StatusCode = (int)HttpStatusCode.InternalServerError;
            Log.Fatal("Custom Exception Message: {Message} StatusCode: {StatusCode} StackeTrace: {StackTrace}", ex.Message, context.Response.StatusCode, ex.StackTrace);
            return context.Response.WriteAsync(JsonConvert.SerializeObject(new { error = "Internal server error" }));
        }
    }
}