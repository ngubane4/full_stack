﻿using Interfaces;
using Interfaces.Repository;
using Microsoft.IdentityModel.Tokens;
using System;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;

namespace API.Helpers
{
    public class JwtAuthenticationManager : IJwtAuthenticationManager
    {
        private readonly string Key;
        private readonly IStudentRepo studentRepo;
        private readonly ISystemRoleRepo systemRole;

        public JwtAuthenticationManager(string key, IStudentRepo studentRepo, ISystemRoleRepo systemRole)
        {
            Key = key;
            this.studentRepo = studentRepo;
            this.systemRole = systemRole;
        }
        
        public string Authenticate(int username, string password)
        {
            var user = studentRepo.GetStudentByStudentNumber(username);
            var userRole = systemRole.GetUSerRole(username);

            if (user == null || !user.Password.Equals(password))
            {
                return null;
            }

            var tokenHandler = new JwtSecurityTokenHandler();
            var tokenKey = Encoding.ASCII.GetBytes(Key);
            var tokenDesriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(new Claim[]
                {
                    new Claim(ClaimTypes.Name, user.Username.ToString()),
                    new Claim(ClaimTypes.Role, userRole),
                    new Claim("ConfidentialAccess", "false")
                }),

                Expires = DateTime.UtcNow.AddHours(1),

                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(tokenKey), SecurityAlgorithms.HmacSha256Signature),

            };

            var token = tokenHandler.CreateToken(tokenDesriptor);

            return tokenHandler.WriteToken(token);
        }
    }
}